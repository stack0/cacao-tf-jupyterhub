variable "username" {
  type = string
  description = "username"
}

variable "project" {
  type = string
  description = "project name"
}

variable "region" {
  type = string
  description = "string, openstack region name; default = IU"
  default = "IU"
}

variable "network" {
  type = string
  description = "network to use for vms"
  default = "auto_allocated_network"
}

variable "instance_name" {
  type = string
  description = "name of jupyterhub instance"
}

variable "instance_count" {
  type = number
  description = "number of instances to launch"
  default = 1
}

variable "image" {
  type = string
  description = "string, image id to launch; either image or image_name should be defined; note, image will take priority over image name"
  default = ""
}

variable "image_name" {
  type = string
  description = "string, image name to launch; either image or image_name should be defined; note, image will take priority over image name"
  default = ""
}


variable "flavor" {
  type = string
  description = "flavor or size for the worker instances to launch"
  default = "m1.tiny"
}

variable "flavor_master" {
  type = string
  description = "flavor or size for the master instance to launch"
  default = "m3.medium"
}

variable "keypair" {
  type = string
  description = "keypair to use when launching"
  default = ""
}

variable "power_state" {
  type = string
  description = "power state of instance"
  default = "active"
}

variable "ip_pool" {
  type = string
  description = "deprecated"
  default = "public"
}

variable "user_data" {
  type = string
  description = "cloud init script"
  default = ""
}

variable "security_groups" {
  type = list(string)
  description = "array of security group names, either as a a comma-separated string or a list(string). The default is ['default', 'cacao-default']. See local.security_groups"
  default = ["default", "cacao-default"]
}

variable "jupyterhub_floating_ip" {
  type = string
  description = "floating ip to assign, if one was pre-created; otherwise terraform will auto create one"
  default = ""
}

variable "jupyterhub_hostname" {
  type = string
  description = "public facing hostname, if set, will be used for the callback url; default is not use set one, which will then use the floating ip"
  default = ""
}

variable "jupyterhub_authentication" {
  type = string
  description = "jupyterhub authentication"
  default = "default"
}

variable "jupyterhub_allowed_users" {
  type = any
  description = "array of allowed users, either as a comma-separated string or a list(string); default is []; see local.jupyterhub_allowed_users"
  # default = []
}

variable "jupyterhub_admins" {
  type = any
  description = "array of admins, either as a comma-separated string or a list(string); default is []; see local.jupyterhub_admins"
  # default = []
}
variable "jupyterhub_dummy_password" {
  type = string
  description = "dummy authentication password"
  default = ""
}
variable "jupyterhub_oauth2_clientid" {
  type = string
  description = "oauth2 client id"
  default = ""
}
variable "jupyterhub_oauth2_secret" {
  type = string
  description = "oauth2 client password"
  default = ""
}

variable "jh_include_dask" {
  type = bool
  description = "bool, will install dask along side jupyterhub"
  default = false
}

variable "dask_gateway_image" {
  type = string
  description = "string, default image to use for dask gateway"
  default = "ghcr.io/dask/dask-gateway:2023.9.0"
}

variable "dask_worker_cores" {
  type = number
  description = "number, cores requested per worker"
  default = 2
}

variable "dask_worker_cores_min" {
  type = number
  description = "number, min cores per worker"
  default = 1
}

variable "dask_worker_cores_max" {
  type = number
  description = "number, max cores per worker"
  default = 4
}

variable "dask_worker_memory" {
  type = number
  description = "number, ram requested per worker"
  default = 2
}

variable "dask_worker_memory_min" {
  type = number
  description = "number, min ram per worker"
  default = 1
}

variable "dask_worker_memory_max" {
  type = number
  description = "number, max ram per worker"
  default = 8
}

variable "jupyterhub_oauth2_callback_https_enable" {
  type = string
  description = "oauth2 callback url enable ssl"
  default = false
}

variable "jupyterhub_singleuser_default_url" {
  type = string
  description = "singleuser default url"
  default = "/lab"
}

variable "jupyterhub_singleuser_image" {
  type = string
  description = "singleuser image"
  default = "jupyter/datascience-notebook"
}

variable "jupyterhub_singleuser_image_tag" {
  type = string
  description = "singleuser image tag"
  default = "latest"
}

variable "do_enable_gpu" {
  type = bool
  description = "boolean, whether to enable gpu components"
  default = false
}

variable "gpu_timeslice_enable" {
  type = bool
  description = "boolean, whether to enable gpu timeslicing (only used if gpu_enable == true)"
  default = true
}

variable "gpu_timeslice_num" {
  type = number
  description = "number, number of time slices if gpu timeslicing is enabled; 0 is default, which means auto-slice (js2 only)"
  default = 0
}

variable "do_ansible_execution" {
  type = bool
  description = "boolean, whether to execute ansible"
  default = true
}

# variable "ansible_execution_dir" {
#   type = string
#   description = "string, directory to execute ansible, including location to create the inventory file, where the requirements file is, etc"
#   default = "./ansible"
# }

variable "jupyterhub_deploy_strategy" {
  type = string
  description = "string, either z2jh_k3s, jh_dockerspawner, z2jh_kubeadm"
  default = "z2jh_k3s"
}

variable "do_jupyterhub_dockerspawner_swarmspawner" {
    type = bool
    description = "boolean, use swarmspawner if true, dockerspawner if false; only used if jupyterhub_deploy_strategy == false; default = true"
    default = true
}

variable "jh_storage_size" {
  type = number
  description = "integer, size in gigabytes for shared files"
  default = -1
}

variable "jh_storage_mount_dir" {
  type = string
  description = "string, the path inside of the container to mount the shared storage"
  default = "/home/jovyan/shared"
}

variable "jh_storage_readonly" {
  type = bool
  description = "bool, will the storage be readonly (currently not used)"
  default = false
}

variable "jh_resources_request_cpu" {
  type = string
  description = "string, representing the amount of vcpu to the hub container, 0m - 1000m"
  default = null
}

variable "jh_resources_request_memory" {
  type = string
  description = "string, representing the amount of memory to the hub container, 200Mi to 4Gi"
  default = null
}

variable "jh_singleuser_cpu_min" {
  type = string
  description = "string, representing the minimum amount of vcpu to the singleuser containers; default is null (0.5)"
  default = null
}

variable "jh_singleuser_memory_min" {
  type = string
  description = "string, representing the minimum amount of memory (in gigabytes) to the singleuser containers; default is null (1G)"
  default = null
}

variable "jh_singleuser_cpu_max" {
  type = string
  description = "string, representing the maximum amount of vcpu to the singleuser containers; default is null or unlimited"
  default = null
}

variable "jh_singleuser_memory_max" {
  type = string
  description = "string, representing the maximum amount of memory (in gigabytes) to the singleuser containers; default is null or unlimited"
  default = null
}

variable "do_jh_singleuser_exclude_master" {
  type = bool
  description = "bool, if true will exclude single user notebooks from master, default is false"
  default = false
}

variable "do_jh_precache_images" {
  type = bool
  description = "bool, if true will precache jupyterlab images; do not set to true for LARGE images"
  default = false
}

variable "fip_associate_timewait" {
  type = string
  description = "number, time to wait before associating a floating ip in seconds; needed for jetstream; will not be exposed to downstream clients"
  default = "30s"
}

variable "root_storage_source" {
  type = string
  description = "string, source currently supported is image; future values will include volume, snapshot, blank"
  default = "image"
}

variable "root_storage_type" {
  type = string
  description = "string, type is either local or volume"
  default = "local"
}

variable "root_storage_size" {
  type = number
  description = "number, size in GB"
  default = -1
}

variable "root_storage_delete_on_termination" {
  type = bool
  description = "bool, if true delete on termination"
  default = true
}
