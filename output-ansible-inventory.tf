resource "local_file" "ansible-inventory" {
    content = templatefile("${path.module}/hosts.yml.tmpl",
    {
        master_ip = var.power_state != "active" ? "0.0.0.0" : (var.jupyterhub_floating_ip == "" ? openstack_compute_floatingip_associate_v2.os_master_floatingip_associate.0.floating_ip : var.jupyterhub_floating_ip)
        master_name = openstack_compute_instance_v2.os_master_instance.0.name
        worker_ips = openstack_compute_floatingip_associate_v2.os_worker_floatingips_associate.*.floating_ip
        worker_names = openstack_compute_instance_v2.os_worker_instance.*.name # we could use this instead of an generically generated index name
        cacao_user = local.system_user
        jupyterhub_authentication = var.jupyterhub_authentication
        jupyterhub_allowed_users = local.jupyterhub_allowed_users
        jupyterhub_admins = local.jupyterhub_admins
        jupyterhub_dummy_password = var.jupyterhub_dummy_password
        jupyterhub_oauth2_clientid = var.jupyterhub_oauth2_clientid
        jupyterhub_oauth2_secret = var.jupyterhub_oauth2_secret
        jupyterhub_singleuser_default_url = var.jupyterhub_singleuser_default_url
        jupyterhub_singleuser_image = var.jupyterhub_singleuser_image
        jupyterhub_singleuser_image_tag = var.jupyterhub_singleuser_image_tag
        jupyterhub_oauth2_callback_https_enable = var.jupyterhub_oauth2_callback_https_enable
        jupyterhub_do_enable_gpu = var.do_enable_gpu
        gpu_timeslice_enable = local.gpu_timeslice_enable
        gpu_timeslice_num = local.gpu_timeslice_num
        jupyterhub_deploy_strategy = var.jupyterhub_deploy_strategy
        jupyterhub_hostname_isset = var.jupyterhub_hostname == "" ? false : true
        jupyterhub_hostname = local.jupyterhub_external_hostname
        do_jupyterhub_dockerspawner_swarmspawner = var.do_jupyterhub_dockerspawner_swarmspawner
        do_share_enable = var.jh_storage_size > 0 ? true : false
        share_size = var.jh_storage_size
        share_access_key = try(openstack_sharedfilesystem_share_access_v2.share_01_access[0].access_key,"")
        share_export_path = try(openstack_sharedfilesystem_share_v2.share_01.0.export_locations[0].path,"")
        share_access_to = try(openstack_sharedfilesystem_share_access_v2.share_01_access[0].access_to,"")
        share_ceph_monitors = try(trimsuffix(openstack_sharedfilesystem_share_v2.share_01.0.export_locations[0].path,":${local.share_ceph_root_path}"),"")
        share_ceph_root_path = local.share_ceph_root_path
        share_readonly = var.jh_storage_readonly
        jupyterhub_share_mount_dir = var.jh_storage_mount_dir
        jupyterhub_resource_request_cpu = var.jh_resources_request_cpu
        jupyterhub_resource_request_memory = var.jh_resources_request_memory
        do_jh_singleuser_exclude_master = var.do_jh_singleuser_exclude_master
        do_jh_precache_images = var.do_jh_precache_images
        jh_singleuser_cpu_min = var.jh_singleuser_cpu_min
        jh_singleuser_memory_min = var.jh_singleuser_memory_min
        jh_singleuser_cpu_max = var.jh_singleuser_cpu_max
        jh_singleuser_memory_max = var.jh_singleuser_memory_max
        jh_include_dask = var.jh_include_dask
        dask_gateway_image = var.dask_gateway_image
        dask_worker_cores = var.dask_worker_cores
        dask_worker_cores_min = var.dask_worker_cores_min
        dask_worker_cores_max = var.dask_worker_cores_max
        dask_worker_memory = var.dask_worker_memory
        dask_worker_memory_min = var.dask_worker_memory_min
        dask_worker_memory_max = var.dask_worker_memory_max
    })
    filename = "${path.module}/ansible/hosts.yml"
}

resource "null_resource" "ansible-execution" {
    count = var.do_ansible_execution && var.power_state == "active" ? 1 : 0

    triggers = {
        always_run = "${timestamp()}"
    }

    provisioner "local-exec" {
        command = "ansible-galaxy install -r requirements.yaml"
        working_dir = "${path.module}/ansible"
    }

    provisioner "local-exec" {
        command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_SSH_PIPELINING=True ANSIBLE_CONFIG=ansible.cfg ansible-playbook -i hosts.yml --forks=10 playbook.yaml"
        working_dir = "${path.module}/ansible"
    }

    depends_on = [
        local_file.ansible-inventory
    ]
}
