output "instance_uuids" {
  value = concat([openstack_compute_instance_v2.os_master_instance.0.id], tolist(openstack_compute_instance_v2.os_worker_instance.*.id))
}

output "github_authorization_callback_url" {
  value = "${local.jupyterhub_external_protocol}${local.jupyterhub_external_hostname}/hub/oauth_callback}"
}

output "jupyterhub_url" {
  value =  "${local.jupyterhub_external_protocol}${local.jupyterhub_external_hostname}"
}

# cacao_output_metadata is a list of a map objects i.e. list(map) or in json parlance, an array of json objects
# downstream clients of the state view can parse the json of this variable
output "cacao_output_metadata" {
  value = jsonencode ([
    {name="name1", value="value1", description="description1"},
    {name="name2", value="value2", description="description2"},
    {name="name3", value="value3", description="description3"}
  ])
}