# cacao-tf-jupyterhub

This repo is a [Cacao](https://gitlab.com/cyverse/cacao)-compliant terraform module to deploy [Zero to Jupyterhub](https://zero-to-jupyterhub.readthedocs.io/en/latest/), equipped with kubernetes (k3s). This currently has been tested on [Jetstream 1](https://www.jetstream-cloud.org) and will be tested on Jetstream 2, once it is available.

## Prerequisites

* Although not a strong requirement, you'll need to pass in the network id or name. The default is `auto_allocated_network` if not specified in the network variable.

`openstack network auto allocated topology create --or-show`

## Installation

To use this terraform module within Cacao, you simply need to import this module in the Templates section of Cacao. If you don't see the menu item for Templates, then you'll need to enable it in the Advance Users section, which is not available yet :).

You can also use this template directly by simply using the terraform cli. You will want to pair the terraform execution with the companion ansible, which is referenced in the `metadata.json`.

Note: Do not use `default` authentication

## Addons

### Dask Gateway

Enabling Dask in the CACAO options will deploy dask gateway along side Jupyterhub.  In order to connect, you'll want to source the CLUSTER-IP of the traefik-dask-gateway service:

```
kubectl get services traefik-dask-gateway --namespace dask-gateway
NAME                   TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
traefik-dask-gateway   ClusterIP   10.43.123.239   <none>        80/TCP    72m
```

With that IP, you can then use a terminal in your notebook to install dask-gateway:
```
pip install dask_gateway
```

And finally establish a demo cluster with this, after adjusting the IP:
```
from dask_gateway import Gateway
gateway = Gateway(
    "http://10.43.123.239",
    auth="jupyterhub",
)
cluster = gateway.new_cluster()
cluster.scale(2)
client = cluster.get_client()
print(cluster)
```

## TODO
* input variable validation using submodule

## Contributing
Contributions are always welcome

## Authors and acknowledgment
This module was created initially by Edwin Skidmore, and maintained by the Cloud Native Services team at [CyVerse](https://www.cyverse.org). Please support our project.